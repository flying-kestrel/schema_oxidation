use proc_macro::TokenStream;
use quote::quote;

pub fn from_string(mod_name: &str, content: &str) -> TokenStream {
    let mod_name = quote::format_ident!("{}", mod_name);

    let all_data: serde_json::Value = serde_json::from_str(content).unwrap();

    let schema = crate::schema::generate_schema(&all_data);

    let ret = quote! {
        mod #mod_name {
            use serde::{Deserialize,Serialize};
            #schema
        }
    };

    ret.into()
}

pub fn from_file(mod_name: &str, path: &str) -> TokenStream {
    let mod_name = quote::format_ident!("{}", mod_name);

    let all_data: serde_json::Value =
        serde_json::from_reader(std::io::BufReader::new(
        std::fs::File::open(path).expect("Unable to open schema")))
            .expect("Schema is invalid JSON");

    let schema = crate::schema::generate_schema(&all_data);

    let ret = quote! {
        mod #mod_name {
            use serde::{Deserialize,Serialize};
            #schema
        }
    };

    ret.into()
}
