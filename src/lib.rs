//! `schema_oxidation`: conversion from JSON schemas to Rust objects.
//!
//! This crate provides a way to automatically generate Rust objects (with serialization /
//! deserialization provided by serde) from a pre-existing JSON schema. Currently only fairly basic
//! support is present, though it is sufficient for simple purposes.
//!
//! The schema JSON can either be loaded [from a file](generate_from_file!) located in the
//! project's source tree, or [from a string](generate_from_string!). The latter is primarily
//! useful for testing purposes, though they both function identically under the hood.

use proc_macro::TokenStream;
use syn;
use syn::parse::Parse;
use syn::Result as SynResult;

mod load;
mod schema;

struct FromParams {
    mod_name: syn::Ident,
    #[allow(dead_code)]
    colon: syn::token::Colon,
    data: syn::LitStr,
}

impl Parse for FromParams {
    fn parse(input: syn::parse::ParseStream) -> SynResult<Self> {
        Ok(FromParams {
            mod_name: input.parse()?,
            colon: input.parse()?,
            data: input.parse()?,
        })
    }
}

/// Generate schema objects from a specified string.
///
/// The resulting objects are placed in a module named by the first parameter, with the schema root
/// object named `SchemaRoot`. All other object types will either be named by the names given
/// explicitly in the schema (e.g. for things defined by `$defs` or `definitions`) or given a name
/// based on the closest named ancestor container and the field name.
///
/// For example:
/// ```rust
/// schema_oxidation::generate_from_string!(example_schema: r#"
/// {
///     "$schema": "https://json-schema.org/draft/2020-12/schema",
///     "type": "object",
///     "properties": {
///         "fieldA": { "type": "int" },
///         "fieldB": { "type": "string" },
///         "fieldC": {
///             "type": "object",
///             "properties": {
///                 "something": { "type": "string" }
///             }
///         }
///     }
/// }
/// "#);
/// ```
///
/// The generated output will be the following, modulo some extra fields to account for extra JSON
/// data that may be passed in data to be deserialized and not specified in the schema:
/// ```rust
/// mod example_schema {
/// use serde::{Serialize,Deserialize};
/// #[derive(Serialize,Deserialize)]
/// pub struct SchemaRoot {
///     pub fieldA: Option<i64>,
///     pub fieldB: Option<String>,
///     pub fieldC: Option<SchemaRoot_fieldC>
/// }
///
/// #[derive(Serialize,Deserialize)]
/// pub struct SchemaRoot_fieldC {
///     pub something: Option<String>
/// }
/// 
/// }
/// ```
#[proc_macro]
pub fn generate_from_string(input: TokenStream) -> TokenStream {
    let parsed: FromParams = syn::parse_macro_input!(input);

    load::from_string(&parsed.mod_name.to_string(), &parsed.data.value())
}

/// Generate schema objects based on JSON loaded from a file, but is otherwise identical to
/// [generate_from_string!].
///
/// Note that the path passed here must be relative to the Cargo project root, or else
/// this will not work.
#[proc_macro]
pub fn generate_from_file(input: TokenStream) -> TokenStream {
    let parsed: FromParams = syn::parse_macro_input!(input);

    load::from_file(&parsed.mod_name.to_string(), &parsed.data.value())
}
