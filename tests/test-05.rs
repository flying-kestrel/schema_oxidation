schema_oxidation::generate_from_string!(schema: r#"
{
    "type": "string"
}
"#);

schema_oxidation::generate_from_string!(schema2: r#"
{
    "type": "object",
    "properties": {
        "a": {
            "type": "string"
        },
        "b": {
            "type": "int"
        }
    }
}
"#);

schema_oxidation::generate_from_string!(schema3: r#"
{
    "type": "object",
    "$defs": {
        "something": {
            "type": "object"
        }
    },
    "properties": {
        "a": {
            "type": "string"
        },
        "b": {
            "type": "int"
        }
    }
}
"#);

schema_oxidation::generate_from_string!(schema4: r#"
{
    "type": "array",
    "description": "something"
}
"#);

schema_oxidation::generate_from_string!(schema5: r#"
{
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "a": {
                "type": "string"
            }
        },
        "requiredProperties": ["a"]
    }
}
"#);

#[test]
fn simple_test() {}
